const assert = require('assert');

const { Builder, By } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const fs = require('fs');
const path = require('path');

describe('Demo Web Shops Test Suite', function () {
    this.timeout(30000); // Adjust the timeout as needed

    let firefoxDriver;
    //this const is shared across 2 tests register and login
    const generatedEmail = `john.doe.${Date.now()}@example.com`;
    console.log('generatedEmail', generatedEmail);

    before(async function () {
        // Initialize Firefox WebDriver
        firefoxDriver = new Builder()
            .forBrowser('firefox')
            //.setFirefoxOptions(new firefox.Options().headless())
            .build();


        await firefoxDriver.manage().window().maximize();
    });

    beforeEach(async function () {
        await firefoxDriver.get('https://demowebshop.tricentis.com');
    });

    after(async function () {
        // Quit the Firefox WebDriver after all tests
        await firefoxDriver.quit();
    });

    it('Verify that allows register a User', async function () {
        const btnRegister = await firefoxDriver.findElement(By.className('ico-register'));
        await btnRegister.click();

        const checkBox = await firefoxDriver.findElement(By.id('gender-male'));
        await checkBox.click();

        const firstName = await firefoxDriver.findElement(By.id('FirstName'));
        await firstName.sendKeys('John');

        const lastName = await firefoxDriver.findElement(By.id('LastName'));
        await lastName.sendKeys('Doe');

        const email = await firefoxDriver.findElement(By.id('Email'));
        await email.sendKeys(generatedEmail);

        const password = await firefoxDriver.findElement(By.id('Password'));
        await password.sendKeys('qwerty90');

        const confirmPassword = await firefoxDriver.findElement(By.id('ConfirmPassword'));
        await confirmPassword.sendKeys('qwerty90');

        const sendFormBtn = await firefoxDriver.findElement(By.id('register-button'));
        await sendFormBtn.click();


        const messageBlock = await firefoxDriver.findElement(By.className('result'));
        const message = await messageBlock.getText();
        assert.strictEqual(message, 'Your registration completed');

        const logout = await firefoxDriver.findElement(By.className('ico-logout'));
        await logout.click();
    });

    it('Verify that allows login a User', async function () {
        const btnLogin = await firefoxDriver.findElement(By.className('ico-login'));
        await btnLogin.click();

        const email = await firefoxDriver.findElement(By.id('Email'));
        await email.sendKeys(generatedEmail);

        const password = await firefoxDriver.findElement(By.id('Password'));
        await password.sendKeys('qwerty90');

        const submitLoginForm = await firefoxDriver.findElement(By.className('login-button'));
        await submitLoginForm.click();

        const accountBlock = await firefoxDriver.findElement(By.className('account'));
        const accountBlockText = await accountBlock.getText();


        assert.strictEqual(accountBlockText, generatedEmail);
    });


});
