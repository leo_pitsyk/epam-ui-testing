const assert = require('assert');

const { Builder, By } = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const fs = require('fs');
const path = require('path');

describe('EPAM.com Test Suite', function () {
    this.timeout(30000); // Adjust the timeout as needed

    let firefoxDriver;

    before(async function () {
        // Initialize Firefox WebDriver
        firefoxDriver = new Builder()
            .forBrowser('firefox')
            //.setFirefoxOptions(new firefox.Options().headless())
            .build();


        await firefoxDriver.manage().window().maximize();
    });

    beforeEach(async function () {
        await firefoxDriver.get('https://www.epam.com');
    });

    after(async function () {
        // Quit the Firefox WebDriver after all tests
        await firefoxDriver.quit();
    });

    it('Check EPAM.com title', async function () {
        const title = await firefoxDriver.getTitle();
        assert.strictEqual(title, 'EPAM | Software Engineering & Product Development Services');
    });

    it('Switch Theme to Opposite State', async function () {
        // Locate and click the theme toggle button
        const themeToggle = await firefoxDriver.findElements(By.className('theme-switcher'));
        await themeToggle[1].click();

        // Add a wait to allow the theme to switch (you might need to adjust the wait time)
        await firefoxDriver.sleep(2000);

        // Verify that the theme has changed to the opposite state
        const header = await firefoxDriver.findElement(By.className('header-ui-23'));
        const headerBackgroundColor = await header.getCssValue('background');
        assert.equal(headerBackgroundColor, 'rgb(251, 250, 250)', 'Dark theme is not applied after switching');
    });

    it('Change language to UA', async () => {
        const languageSelector = await firefoxDriver.findElement(By.className('location-selector-ui-23'));
        await languageSelector.click();

        const languageUaOption = await firefoxDriver.findElement(By.css('[class="location-selector__link"][lang="uk"]'));
        await languageUaOption.click();

        assert.strictEqual(await firefoxDriver.getCurrentUrl(), 'https://careers.epam.ua/', 'Language is not changed to UA');
    });

    it('Include expected items in the policies list', async () => {
        // Scroll to the bottom of the page
        await firefoxDriver.executeScript('window.scrollTo(0, document.body.scrollHeight);');

       // Find the policies section
        const policiesSection = await firefoxDriver.findElement(By.css('.heading.large-preheader'));
        assert.ok(await policiesSection.isDisplayed(), 'Policies section is not displayed');

        // Find the policies list elements
        const policiesListElements = await firefoxDriver.findElements(By.css('div.policies-links-wrapper .fat-links'));

        // Extract the text content of each policy link
        const policiesList = await Promise.all(policiesListElements.map(async (policyElement) => {
            return await policyElement.getText();
        }));

        // Expected policies list items
        const expectedPolicies = [
            'INVESTORS',
            'OPEN SOURCE',
            'PRIVACY POLICY',
            'COOKIE POLICY',
            'APPLICANT PRIVACY NOTICE',
            'WEB ACCESSIBILITY'
        ];

        // Verify that the policies list includes the expected items
        expectedPolicies.forEach((expectedPolicy) => {
            assert.ok(policiesList.includes(expectedPolicy), `Policy "${expectedPolicy}" is missing`);
        });
    });

    it('Allow to switch location list by region', async function () {
        // Navigate to the Our Locations part
        const ourLocationsHeader = await firefoxDriver.findElement(By.id('id-890298b8-f4a7-3f75-8a76-be36dc4490fd'));
        await firefoxDriver.executeScript('arguments[0].scrollIntoView();', ourLocationsHeader);


        // Find the region selector
        const regionSelector = await firefoxDriver.findElement(By.className('tabs-23__ul js-tabs-links-list'));

        // Verify that the regions [AMERICAS, EMEA, APAC] are presented
        const availableRegions = ['AMERICAS', 'EMEA', 'APAC'];
        const regionOptions = await regionSelector.findElements(By.css('.tabs-23__title.js-tabs-title'));

        const actualRegions = await Promise.all(regionOptions.map(async (regionElement) => {
            return await regionElement.getText();
        }));

        assert.deepStrictEqual(actualRegions, availableRegions, 'Regions are not presented as expected');

        // scroll to the region selector
        // Scroll to the regionOption
        await firefoxDriver.executeScript('arguments[0].scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });', regionOptions[0]);
        await firefoxDriver.sleep(2000);
        const bannerAcceptBtn = await firefoxDriver.findElement(By.id('onetrust-accept-btn-handler'));
        await bannerAcceptBtn.click()
        await firefoxDriver.sleep(2000);

        // click on america region
        await regionOptions[0].click();
        //check that link has a new class active
        const activeRegion = await regionOptions[0].getAttribute('class');
        assert.deepStrictEqual(activeRegion, 'tabs-23__title js-tabs-title active', 'Region is not active');
        // click on emea region
        await regionOptions[1].click();
        //check that link has a new class active
        const activeRegion1 = await regionOptions[1].getAttribute('class');
        assert.deepStrictEqual(activeRegion1, 'tabs-23__title js-tabs-title active', 'Region is not active');
        // click on apac region
        await regionOptions[2].click();
        //check that link has a new class active
        const activeRegion2 = await regionOptions[2].getAttribute('class');
        assert.deepStrictEqual(activeRegion2, 'tabs-23__title js-tabs-title active', 'Region is not active');
    });

    //Check the search functionality
    it('Check the search functionality', async function () {
        // Find the search icon
        const searchIcon = await firefoxDriver.findElement(By.className('header-search__button header__icon'));
        await searchIcon.click();

        // Find the search input
        const searchInput = await firefoxDriver.findElement(By.id('new_form_search'));
        await searchInput.sendKeys('AI');

        // Find the search button
        const searchButton = await firefoxDriver.findElement(By.className('custom-search-button'));
        await searchButton.click();
        await firefoxDriver.sleep(2000);

        // Verify that the search results page is opened
        const searchResultsPage = await firefoxDriver.findElement(By.className('search-results__counter'));
        //parse the firts number from the string
        const searchResults = await searchResultsPage.getText();
        const searchResultsNumber = parseInt(searchResults.match(/\d+/)[0]);
        console.log('Search results number: ' + searchResultsNumber);
        assert.ok(searchResultsNumber > 0, 'Search results are not displayed');
    });

    /*check the form required fields
    it('Check the form required fields', async function () {
        await firefoxDriver.get('https://www.epam.com/about/who-we-are/contact');
        await firefoxDriver.sleep(2000);

        const bannerAcceptBtn = await firefoxDriver.findElement(By.id('onetrust-accept-btn-handler'));
        await bannerAcceptBtn.click()
        await firefoxDriver.sleep(2000);
        // Find the contact us link
        const checkBox = await firefoxDriver.findElement(By.name('gdprConsent'));
        await firefoxDriver.executeScript('arguments[0].scrollIntoView({ behavior: "smooth", block: "center", inline: "center" });', checkBox);
        await firefoxDriver.sleep(20000);
        await checkBox.click();


        await firefoxDriver.sleep(5000)
    });*/

    //Check tha the Company logo on the header lead to the main page
    it('Check tha the Company logo on the header lead to the main page', async function () {
        await firefoxDriver.get('https://www.epam.com/about');
        // Find the company logo
        const companyLogo = await firefoxDriver.findElement(By.className('header__logo-container desktop-logo'));
        await companyLogo.click();

        // Verify that the main page is opened

        assert.equal('https://www.epam.com/', await firefoxDriver.getCurrentUrl(), 'Main page is not displayed');
    });


    //Check that allows to download report
    it('Check that allows to download report', async function () {

        await firefoxDriver.get('https://www.epam.com/about');
        // Find the company logo
        const downloadReport = await firefoxDriver.findElement(By.css('[href="https://www.epam.com/content/dam/epam/free_library/EPAM_Corporate_Overview_Q3_october.pdf"]'));
        await downloadReport.click();

        //TODO : change downloadPath to your download folder or even better set specific folder for tests through config options
        const downloadPath = '/Users/nastyasmetanina/Downloads';

        const expectedFileName = 'EPAM_Corporate_Overview_Q3_october.pdf';
        const filePath = path.join(downloadPath, expectedFileName);

        console.log(filePath);
        // Check if the file exists
        const isFileDownloaded = fs.existsSync(filePath);

        // Assert that the file is downloaded
        assert.equal(isFileDownloaded, true, 'Report is not downloaded at '+ filePath + '. Check that const downloadPath is set to YOUR download folder');
    });
});
